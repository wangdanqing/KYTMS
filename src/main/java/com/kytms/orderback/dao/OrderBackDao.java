package com.kytms.orderback.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2018-01-19
 */
public interface OrderBackDao<OrderBack> extends BaseDao<OrderBack> {
}
